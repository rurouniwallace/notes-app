// en.js
const en = {
    'username': 'username',
    'password': 'password',
    'passwordConfirm': 'confirm password',
    'givenName': 'First Name',
    'surname': 'Last Name',
    'phone': 'Phone Number',
    'welcome': 'Welcome to Notes By Rurouniwallace',
    'register': 'New User? Sign Up',
    'forgot_password': 'Forgot Password',
    'login': 'Login',
    'invalid_credentials': 'Username or password was incorrect',
    'username_blank': 'Username must not be blank',
    'password_blank': 'Password must not be blank',
    'passwordConfirmBlank': 'Confirm password must not be blank',
    'passwordConfirmationMismatch': 'Password confirmation must match password',
    'registerSubmit': 'Submit Registration',
    'emailAlreadyRegistered': 'A user with that email address already exists',
    'registrationSuccess': 'You have been successfully registered! You can now log in with your credentials.',
    'returnToLoginLink': 'Click here to return to login.',
    'search': 'Search',
    'new_note': 'New Note',
    'refresh': 'Refresh',
    'all_notes': 'All Notes',
}

export { en };