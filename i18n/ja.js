// ja.js
const ja = {
    'username': 'ユーザ名',
    'password': 'パスワード',
    'passwordConfirm': 'パスワードを再入力',
    'givenName': '名前',
    'surname': '苗字',
    'phone': '電話番号',
    'welcome': 'Notes By Rurouniwallaceへようこそ',
    'register': 'サインアップ',
    'forgot_password': 'パスワードを忘れた場合',
    'login': 'ログイン',
    'invalid_credentials': 'ユーザ名かパスワードか間違います',
    'username_blank': 'ユーザ名を入力してください',
    'password_blank': 'パスワードを入力してください',
    'passwordConfirmBlank': 'パスワードを再入力してください',
    'passwordConfirmationMismatch': '再入力したパスワードが違います',
    'registerSubmit': 'サインアップ',
    'emailAlreadyRegistered': 'このメールアドレスがもう登録しています',
    'registrationSuccess': '登録完了です！ユーザ名・パスワードで認証することができます。',
    'returnToLoginLink': 'ログインへ戻る。',
    'search': '探す',
    'new_note': 'ノートを書く',
    'refresh': 'リフレッシュ',
    'all_notes': '全部',
}

export { ja };