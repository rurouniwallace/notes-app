import { SELECT_NOTE } from "./types"
import { DESELECT_NOTE } from "./types"
import { SELECT_FOLDER } from "./types"
import { DESELECT_FOLDER } from "./types"


export const selectNote = note => {
    return {
        type: SELECT_NOTE,
        payload: note,
    }
}

export const deselectNote = note => {
    return {
        type: DESELECT_NOTE,
        payload: note,
    }
}

export const selectFolder = folder => {
    return {
        type: SELECT_FOLDER,
        payload: folder,
    }
}

export const deselectFolder = note => {
    return {
        type: DESELECT_FOLDER,
        payload: folder,
    }
}