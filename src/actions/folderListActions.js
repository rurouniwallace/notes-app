import { REFRESH_FOLDERLIST } from "./types";

export const refreshFolderList = folderList => {
    return {
        type: REFRESH_FOLDERLIST,
        payload: folderList,
    }
}