export const SELECT_FOLDER = 'SELECT_FOLDER';
export const DESELECT_FOLDER = 'DESELECT_FOLDER';
export const SELECT_NOTE = 'SELECT_NOTE';
export const DESELECT_NOTE = 'DESELECT_NOTE';

export const REFRESH_FOLDERLIST = 'REFRESH_FOLDERLIST';