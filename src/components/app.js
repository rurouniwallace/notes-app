import React from 'react';
import Login from './login';
import Home from './home';
import Register from './register';
import { BrowserRouter, Route } from 'react-router-dom';


export default class App extends React.Component {

    render() {
        return (
            <BrowserRouter>
                <Route exact path="/" component={Home} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/folder/:id" component={Home} />
            </BrowserRouter>
        );
    }
}