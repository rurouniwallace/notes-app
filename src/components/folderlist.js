import React from 'react';
import i18n from "i18n-web";
import {connect} from "react-redux";

export class Folderlist extends React.Component {

    selectFolder(folderId) {
        console.log('Folder selected ' . folderId);
    }

    render() {
        const folderLinks = [];

        folderLinks.push(<li><a href='javascript:void(0);' className='folder-link'>{i18n('all_notes')}</a></li>)
        for (let i = 0; i < this.props.folders.length; i++) {
            const folder = this.props.folders[i];
            folderLinks.push(<li><a id={folder.id} href='javascript:void(0);' className='folder-link' onClick={this.selectFolder(folder.id)}>{folder.name}</a></li>);
        }

        return (
            <ul className='folder-list'>
                {folderLinks}
            </ul>
        );
    };
}

const mapStateToProps = (state) => {
    return {
        folders: state.folderList.folderList,
    };
};

export default connect(mapStateToProps)(Folderlist);