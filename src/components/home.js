import React from 'react';
import {getSessionToken, getUserId} from '../services/login';
import {Topbar} from './topbar';
import Folderlist from './folderlist';
import {getFolders} from "../services/folder";
import {refreshFolderList} from "../actions/folderListActions";
import {connect} from "react-redux";
import {
    BrowserRouter as Router,
    Link,
    Route,
    Routes,
    useParams,
} from "react-router-dom";



export class Home extends React.Component {
    render() {
        const userId = getUserId();

        getFolders(userId, (response) => {
            this.props.refreshFolderList(response.data.folders);
        }, (error) => {
            console.log("an error occurred retrieving folders");
            console.log(error);
        });

        const sessionToken = getSessionToken();

        if (!sessionToken) {

            // this is kinda sketchy. There has to be a better way to do this
            this.props.history.push('/login');
        }

        return (
            <div className="homepage">
                <Topbar></Topbar>
                <Folderlist></Folderlist>
            </div>
        );
    }
}

const mapDispatchToProps = {
    refreshFolderList,
};

export default connect(null, mapDispatchToProps)(Home);