import React from 'react';

// SASS is the default import, so we have to specifically path to the css when importing
import 'bulma/css/bulma.css';
import intlTelInput from 'intl-tel-input';

export default class Input extends React.Component {
    componentDidMount = () => {
        if (this.props.isPhone) {
            const input = document.querySelector(`#${this.props.id}`);
            intlTelInput(input, {
                allowDropdown: false,
                formatOnDisplay: true,
                separateDialCode: true,
            });
        }
    }

    render() {
        let errorMessage = null;
        let inputIcon = '';
        let hasIconClass = '';
        let inputErrorClass = '';
        if (this.props.error) {
            errorMessage = <label htmlFor={this.props.id} className="help is-danger">{this.props.error}</label>
            inputIcon = <span className="icon is-small is-left"><i className="fas fa-user"></i></span>
            inputErrorClass = 'is-danger';
        }

        if (inputIcon) {
            hasIconClass = 'has-icons-right';
        }

        return (
            <div className={`field`}>
                <label className='label' htmlFor={this.props.id}>{this.props.name}</label>
                <div className={`control ${hasIconClass}`}>
                    <input className={`input ${inputErrorClass}`} type={this.props.type} id={this.props.id} placeholder={this.props.name} name={this.props.name} onChange={this.props.onchange}/>
                    {inputIcon}
                </div>
                {errorMessage}
            </div>
        );
    }
}