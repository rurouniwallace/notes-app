import React from 'react';
import Input from './input';
import {Link} from 'react-router-dom';
import '../styles/style.less';
import {doLogin} from '../services/login.js';

// SASS is the default import, so we have to specifically path to the css when importing
import 'bulma/css/bulma.css';
import i18n from 'i18n-web';

export default class Login extends React.Component {
    state = {
        username: "",
        password: "",
        usernameError: "",
        passwordError: "",
        loginError: ""
    };

    checkInputs = () => {
        let usernameError = "";
        let passwordError = "";
        let hasErrors = false;
        if ((!this.state.username || this.state.username.length === 0 )) {
            usernameError = i18n("username_blank");
            hasErrors = true;
        }

        if ((!this.state.password || this.state.password.length === 0 )) {
            passwordError = i18n("password_blank");
            hasErrors = true;
        }

        this.setState({
            usernameError: usernameError,
            passwordError: passwordError
        });

        return hasErrors;
    }

    submitCredentials = () => {
        const hasErrors = this.checkInputs();

        if (hasErrors) {
            this.setState({
                'loginError': ''
            });
            return;
        }

        doLogin(this.state.username, this.state.password, this.handleLoginResponse, this.handleLoginError);
    }

    handleLoginResponse = (response) => {
        this.props.history.push('/');
    }

    handleLoginError = (error) => {

        if (error.response.status == 403) {
            this.setState({
                'loginError': i18n('invalid_credentials'),
            });
        }
    }

    render() {
        let loginErrorMessage = '';

        if (this.state.loginError) {
            loginErrorMessage = <p className="notification is-danger is-light">{this.state.loginError}</p>


        }

        return (
            <div className="login-screen">
                <div className="login-container column">
                    <div className="login-form">
                        {loginErrorMessage}
                        <Input id="username" name={i18n('username')} type="text" error={this.state.usernameError} onchange={event => {this.setState({ username: event.target.value });}}/>
                        <Input id="password" name={i18n('password')} type="password" error={this.state.passwordError} onchange={event => {this.setState({ password: event.target.value });}}/>
                        <button className="button is-primary is-fullwidth" onClick={this.submitCredentials}>
                            {i18n('login')}
                        </button>
                        <ul className="login-help">
                            <li>
                                <Link to="register">{i18n('register')}</Link>
                            </li>
                            <li>
                                <Link to="forgot-password">{i18n('forgot_password')}</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}