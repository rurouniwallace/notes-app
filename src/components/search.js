import React from 'react';
import {selectNote} from '../actions/elementSelectActions';
import {connect} from 'react-redux';
import i18n from "i18n-web";
import {faMagnifyingGlass} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export class Search extends React.Component {
    submitSearch = () => {
        this.props.selectNote({
            title: 'Redux Test'
        });
    }

    render() {
        this.props.selectNote({
            title: 'Redux Test'
        });
        return (
            <div className="search-container">
                <div className="field has-addons">
                    <div className="control input-box">
                        <input className="input" placeholder={i18n('search')} onChange={this.props.changeHandler} />
                    </div>
                    <div className="control search-button">
                        <a className="button is-info" onClick={this.submitSearch}>
                            <FontAwesomeIcon icon={faMagnifyingGlass} alt={i18n('search')}/>
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = {
    selectNote,
};

export default connect(null, mapDispatchToProps)(Search);