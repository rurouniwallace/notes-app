import React from 'react';
import {connect} from 'react-redux';
import Search from './search';
import i18n from "i18n-web";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCirclePlus, faArrowsRotate } from '@fortawesome/free-solid-svg-icons';

export class Topbar extends React.Component {
    render() {
        return (
            <header id='topbar'>
                <div class='title-box'>
                    <FontAwesomeIcon icon={faArrowsRotate} alt={i18n('refresh')}/>
                    <h1>{this.props.selectedNote != null ? this.props.selectedNote.title : ''}</h1>
                    <FontAwesomeIcon icon={faCirclePlus} alt={i18n('new_note')}/>
                </div>
                <Search></Search>
            </header>
        );
    }
}

const mapStateToProps = (state) => {
    return {
    selectedFolder: state.elementSelect.selectedFolder,
    selectedNote: state.elementSelect.selectedNote,
    };
};

export default connect(mapStateToProps)(Topbar);