import { SELECT_FOLDER } from "../actions/types"
import { DESELECT_FOLDER } from "../actions/types"
import { SELECT_NOTE } from "../actions/types"
import { DESELECT_NOTE } from "../actions/types"

const initialState = {
    selectedNote: null,
    selectedFolder: null,
}

export default function(state = initialState, action) {
    switch (action.type) {
        case SELECT_FOLDER:
            return {
                selectedFolder: action.payload,
                selectedNote: state.selectedNote,
            };
            break;
        case DESELECT_FOLDER:
            return {
                selectedFolder: null,
                selectedNote: state.selectedNote,
            };
            break;
        case SELECT_NOTE:
            return {
                selectedFolder: state.selectedFolder,
                selectedNote: 'action.payload',
            };
            break;
        case DESELECT_NOTE:
            return {
                selectedFolder: state.selectedFolder,
                selectedNote: null,
            };
            break;
        default:
            return state;
    }
}