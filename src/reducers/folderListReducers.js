import {REFRESH_FOLDERLIST} from "../actions/types"

const initialState = {
    folderList: []
}

export default function(state = initialState, action) {
    switch (action.type) {
        case REFRESH_FOLDERLIST:
            return {
                folderList: action.payload
            }
            break;
        default:
            return state;
    }
}