import { combineReducers } from "redux";
import elementSelectReducers from "./elementSelectReducers";
import folderListReducers from "./folderListReducers";

const allReducers = combineReducers({
    elementSelect: elementSelectReducers,
    folderList: folderListReducers,
});

export default allReducers;