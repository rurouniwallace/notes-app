import axios from 'axios';
import {config} from '../config.js';

export function getFolders(userId, responseHandler, errorHandler) {
    axios.get(`${config.hostname}/users/${userId}/folders`, {withCredentials: true}).then(responseHandler).catch(errorHandler);
}