import axios from 'axios';
import {config} from '../config.js';
import Cookies from 'cookies-js';
import * as jose from 'jose';

export function doLogin(username, password, responseHandler, errorHandler) {
    const requestBody = {
        'email': username,
        'password': password,
    };

    axios.post(`${config.hostname}/auth`, requestBody, {withCredentials: true}).then(responseHandler).catch(errorHandler);
    console.log(`username: ${username}, password: ${password}, config: ${config}`);
}

export function getSessionToken() {
    return Cookies.get('session');
}

export function getUserId() {
    const sessionToken = Cookies.get('session');

    return (sessionToken != null) ? peekClaimsFromJwt(sessionToken, 'sub') : null;

}

function peekClaimsFromJwt(jwt, key) {
    const claims = jose.decodeJwt(jwt);
    return claims[key];
}