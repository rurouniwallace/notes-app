import axios from "axios";
import {config} from "../config";


export function searchNotes(userId, searchQuery, numResults, responseHandler, errorHandler) {
    axios.get(`${config.hostname}/user/${userId}/notes/search?query=${searchQuery}&numResults=${numResults}`,{withCredentials: true}).then(responseHandler).catch(errorHandler);
}