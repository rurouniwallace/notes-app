import axios from 'axios';
import {config} from '../config.js';

export function registerUser(user, responseHandler, errorHandler) {
    const requestBody = user;

    axios.post(`${config.hostname}/users`, requestBody,  {withCredentials: true}).then(responseHandler).catch(errorHandler);
}